({
doInit: function(component, event, helper) {
    var lat;
    var lng;
    
    var recId = component.get("v.recordId"); 
    console.log(recId);
    
    var action = component.get("c.getAddressFields");
    action.setParams({ "recsId" : recId });
    action.setCallback(this,function(res){
        
        var state = res.getState();
        if(state === "SUCCESS"){
            console.log(state);
            var str = res.getReturnValue();
            component.set('v.availvals',str);
        }
    });
    $A.enqueueAction(action);
},
    onSingleSelectChange : function(component, event, helper)
    {
        var lat;
        var lng;
        var recId = component.get("v.recordId"); 
   	    console.log(recId);
		var map = null;
        var leafl;
        document.getElementById('mapnew').innerHTML ="";
        document.getElementById('mapnew').innerHTML = "<div style=\"height: 300px\" class=\"map\" id=\"map\"></div>";
	
        var selected = component.find("getsel").get("v.value");
            var action = component.get("c.PopulateLatituteLongitude");
   			action.setParams({ 
                "recsId" : recId ,
                "opt" : selected
            });
            action.setCallback(this,function(res){
            var state = res.getState();
            if(state === "SUCCESS"){
                    console.log(state);
                    var str = res.getReturnValue();
                    var arr = str.split(";");
                    lat = parseFloat(arr[0]);
                    lng = parseFloat(arr[1]);
                    setTimeout(function() {
                
                        map=new L.map('map', {zoomControl: true});
                        map.setView(new L.LatLng(lat, lng), 16);
                        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
                            {
                                attribution: 'Tiles © Esri'
                            }).addTo(map);

                        // Add marker
                        L.marker([lat, lng]).addTo(map)
                            .bindPopup('The Location');
                        map.invalidateSize();
                        console.log(map);
            });
                         
        }
    });
    $A.enqueueAction(action);
        
}
})