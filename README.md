#README#
This repository implements a lightning map component that takes the address value from the address fields available in that object.

###What is this repository for?###
Use it to create a lightning map component that can be placed on any object record detail page. 
The user can select the address field which should be displayed on the map from the picklist available in the component.

###How do I get set up?###
This is an example code. Modify as per need. In the example, the code lists all the address fields (Standard or Custom) available in that object. 
Based on the user's selection, the field value is queried in the apex controller and the address is passed back to the component so that it can be displayed on
the map.

Upload Leaflet js library as a static resource called leaflet. Download link: http://cdn.leafletjs.com/leaflet/v1.1.0/leaflet.zip

Leaflet Website: http://leafletjs.com


###Limits###
1) The address stored in the field should contain only spaces. No new line characters.
2) As it makes a REST API call to google map apis, the exception raised when no matching address is found is not handled.

###Who do I talk to?###
radhika.shet@absyz.com

You can also refer to the blog : 
https://teamforcesite.wordpress.com/2017/07/25/configurable-map-component-for-lightning-record-pages/


